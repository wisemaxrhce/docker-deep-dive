
---
# Dockerfile

---

# Intro To The Dockerfile

What is dockerfile?

Docker files are instructions on how to build an image.

The file container all commands used to start a container:

- Docker image consists of read-only layers
- Each layer represents a dockerfile instruction.
- Layers are stacked.
- Each layer is delta of change from the previous layer
- Images are build with **docker image build** command

---

# Intro To The Dockerfile (cont.)

General guidelines for dockerfile creation:

- Keep containers as ephemeral as possible
- Avoid including unnecessary files
- Use **.dockerignore** to dismiss things you do not wish to be included in your image
- Decouple application, no monoliths inside containers
- Use multi-stage container builds.
- Don't install unnecessary packages. basically what ever is nice to have, does not belong in container.
- Minimize number of layers
- Sort multi-line arguments
- Leverage the builds

---

# Working With Instructions

Here is a mostly complete list of docker file commands.

- FROM: it sets the base image for subsequent instructions
- MAINTAINER: it sets the author field of the generated images.
- RUN: it will execute any commands when docker image will be created.
- CMD: it will execute any commands when docker container will be executed. CMD can only be used once per dockerfile. to execute multiple commands we have ENTRYPOINT
- LABEL: it adds metadata to an image.
- EXPOSE: it informs docker that the container will listen on the specified network ports at runtime.
- ENV:  it sets the environment variable.
- ADD:  it copies new files, directories or remote file urls.

---
# Working With Instructions (cont.)


- COPY: It copies new files or directories.
the differences of **ADD** are that it's impossible to specify remote URL and also it will not extract archive files automatically. 
- WORKDIR: it sets the working directory
- ARG: it defines a variable that users can pass at build-time to builder with docker build command using the --build-arg VAR=VAL flag
- ONBUILD: it adds a trigger instruction to the image that executes when the image is used as the base for another build

---
# Working With Instructions (cont.)

- HEALTHCHECK: it tells docker how to test a container that it is still working
- ENTRYPOINT: it will execute any commands when docker container will be executed 
- SHELL: it allows the default shell used for shell form of commands to be overridden

> `[!]` Note: there is no point reading what these commands do without using them. thus lets build the container for very simple application to run on  nginx based container.

---

# Working With Instructions (cont.)

Lets start with something simple:
 
Simple dockerfile that copies **FROM** ubuntu image, uses **RUN** to get updated and install apache2 web-server. by the, it runs **CMD** apache web server with `apachectl` with parameters

```docker
FROM ubuntu 
RUN apt-get update && apt-get -y install apache2 
CMD apachectl -D FOREGROUND
```

---
# Working With Instructions (cont.)

We can build it with command below:

**-t** is used to create tags on images

```sh
bash~$ docker build -t apache2:v1 .
```

Congratulations: you are now a owner of a simple image.

> `[!]` Note: Do NOT forget `dot` at the end of the command line 

---

# Working With Instructions (cont.)

Now lets do expand little bit. lets add environment variables with **ENV** keyword.

```docker
FROM ubuntu 
ENV DEBIAN_FRONTEND="noninteractive" 
ENV TZ=Asia/Jerusalem
RUN apt-get update && apt-get -y install apache2
CMD apachectl -D FOREGROUND 
```

And it build it with another version of build command

```sh
bash~$ docker build -t apache2:v2 - < Dockerfile
```

---

# Working With Instructions (cont.)

 <!-- let us continue by working  and adding additional commands with **ENTRYPOINT**, which is used to trigger any command once the container starts
 -->
In this section lets see how we can stop using default root user with **USER** option

```docker
FROM centos:latest 
RUN user -ms /bin/bash botex 
USER botex 
```

In the code above we have built an image that runs in the container as regular user named **botex**.
Once user is created, all the command, such as CMD and ENTRYPOINT that will follow will work under **botex** user. 

---

# Working With Instructions (cont.)


Here is a small example of single html file application that only shows time and with it we have simple nginx based container that we implement in order create container for our use.

```docker
FROM nginx 
LABEL io.vaiolabs.botex=v0.1 
COPY index.html /usr/share/nginx/html 
EXPOSE 8080 
```

> `[!]` Note: the app can be found in presentation folder incase you don't wish to write it down yourself.

---

# Working With Instructions (cont.)

In order to create our own image, we'll have to use the **docker build** command
which will help us in our task.

Once build it done the image is generated. you can try to create container from that specific image.

```sh
bash~$ docker container run -d --name date-app-v0.1 438c78fd66d8
```

> `[!]` Note: the id, belongs to image that you generated and might be different in your case.

---

# Practice

- Create Docker file with:
  -  Create ssl certificate with openssl with example from [here](https://stackoverflow.com/questions/10175812/how-to-generate-a-self-signed-ssl-certificate-using-openssl).
  -  port 8443 external and port 443 internal configured
  -  Copy ssl cert to nginx folder where the storage is suppose to be.
- Build the image naming it self-signed-nginx:v1
- Run the container
  - Check from external access whether you are getting correct ssl access or not.

---

# Environment Variables

<!-- when working on project we tend to over work on services  that container provides us. in other cases to by pass those instance, we add conf files to solve our issues. although we mentioned in storage management of docker that it is possible to bind conf files into our container, in many of the cases it will create **configuration drift**, which we'll have to patch some how.

that is why environment variables were created. with environment variables we can create configuration changes on the fly and those will be only change done, with out creating any conf file for the application. -->
Set the environment variable <key> to the value <value>. This value will be in the environment for all subsequent instructions in the build
Usage:

```sh
ENV `key`=`value`
```

Example:
```sh
ENV MY_NAME="John Wick"
```

Or:

```sh
ENV MY_NAME="John Wick" MY_DOG=Daisy\ The\ Dog \
```

---

# Environment Variables (cont.)


The environment variables set using ENV will persist when a container is run from the resulting image
We can alter the environment variable with --env flag e.g.

```docker
FROM nginx 
COPY index.html /usr/share/nginx/html 
ENV PORT 8080
EXPOSE $PORT 
CMD nginx -g "daemon off;" 
```

And to build with

```sh
bash~$ docker image build .
```
---

# Build Arguments
Build arguments allow us to set build time variables which can be overridden when building the image

For example
```docker
FROM nginx
LABEL org.vaiolabs.io=v0.1
ARG PORT
ARG SRC_DIR /usr/share/nginx/html

COPY index.html $SRC_DIR
EXPOSE $PORT
CMD nginx -g "daemon off;"
```

And of course the build command will look like below:

```sh
bash~$ docker image build --build-arg PORT=8090 .
```

---
# Practice

- Set env variable with docker file
- Set env vars with .env file


---

# Working With Non-Privileged User

When working with containers security is something we all wish to comply with. as such, some of the security measures that are taken in order to assure that **root** user inside the container will not be implemented. as such we have **USER** commands that we can use to assure it

For example:

```docker
FROM centos:7 
RUN useradd -ms /bin/bash app_user
USER app_user 
```

> `[!]` Note: once the docker image with **non-root** user is created, everything in that container will run, under that specific user. if **su** is implemented, you won't have password, thus won't be able to login. if **sudo** is implemented, it will be denied by the container itself.

> `[!]` Note: it will be possible to access the container with **root** from *docker* command with use of **exec -u 0** while creating the container.


---

# Practice

- Create Dockerfile that sets use `snek` in container
- Create `hello-world` python3 app that runs only with user `snek`
  - In case of other user or user root, the app should exit with error

---

# Order Of Executions

Obviously we have gone through some commands already one thing still remains a question: is there any type of hexarchy between commands ?
To be precise, there is none. any command be executed at any time. the **problem** that can occur is problem bad planning. for example:

```docker
FROM centos:7
RUN useradd -ms /bin/bash app_user
USER app_user
RUN mkdir -p ~/app_dir
RUN mkdir -p /etc/app_dir
RUN echo "root_value: True" > /etc/app_dir/app.conf
```

In example above we are creating local user name *app_user*. after the user is created, all the commands are executed with that user. but because of that user being regular user, he does not have any permissions to work on anything. thus the build of that image will fail.


---

# Using Volume Instruction

There are some use cases where an image might need to be created while it already has its own volume attached to it. as such we have **VOLUME**  command for dockerfile.

```docker 
FROM nginx:latest
VOLUME ["/usr/share/nginx/html"]
```

> `[!]` Note: several volumes can be attached at the same time. just square brackets and separate values with commas
> `[!]` Note: you should have volume planned before implementing this use case.

---

# Practice

- Create Dockerfile of existing app , that creates storage when even the container runs.
  - Please use [This](https://gitlab.com/silent-mobius/html-app.git) app as existing application, in case you do not have your own. <!-- TODO html page in private that needs local bind storage-->
  - Build image, and inspect its storage configuration.
  - Connect to container and validate that storage exists and you can use it.

---

# Entrypoint VS. CMD

- CMD is defined as main command of running container
    - it's main use to be main process in the container
    - it is easily overridden
- ENTRYPOINT defines a container with a specific executable
    - it can **NOT** be overridden once the container starts
    - there is a by pass flag called --entrypoint 

The rule of thumb would be to use CMD for easily set env variables and ENTRYPOINT to run the executables

---

# Practice

- Create shell script that runs infinitely with 2 sec sleep between each loop.
- Create Docker file that :
  - Copy the shell script to /bin directory.
  - Run shell script as entry point.
- Build and test it.

---

# Using `.dockerignore`

Just like with **git**, while working on **dockerfile** wen encounter situations in which some of the files that  we work on, either are not needed  orw were created for testing purpose only, thus we need to discard them. in big projects, removing un-necessary files might become tedious, so **.dockerignore** file was introduced. main point of it is to hold the list of things you do not wish to be included in you docker image.
it usually looks like this:

```sh
bash~$ vi .dockerignore
```

The content inside the file can be written in several ways just like in **.gitignore**

```sh
\*/\*.md
\*/tests/\*
\*/.git
```

---

# Practice

- Create script that runs while loop iterating numbers and every time the number is divided by 7 has no remained, create file name `tmp_NUMBER` NUNMBER being the value of the iterator.
- Test the script
- Create Dockerfile that copies and runs the script inside container.
- Create `.dockerignore` file that will ignore all `tmp_NUMBER` files.

---

# Summary Practice

- Create JS/Python/Ruby/Groovy/Bash single file application.
- Create Dockerfile that has pre-defined volume and pre-defined network that it is suppose to connect to.
- Build Image and Run it so it would `mount` the single file application to container.
- Validate that container is on correct pre defined network.
- Save the image to output file.
- BONUS: Create shell script that will build Dockerfile, or run container of the image, based on parameters provided to it.
